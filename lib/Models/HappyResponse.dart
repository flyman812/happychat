import 'package:happychat/Models/childs/MetaModel.dart';

class HappyResponse {
  Meta? meta;
  Map<String,dynamic>? data;

  HappyResponse();
  HappyResponse.fromJson(Map<String, dynamic>? json) {
    meta = json?["meta"] == null ? null : Meta.fromJson(json?["meta"]);
    data = json?["data"] == null ? null : json?["data"];
  }

  Map<String, dynamic> toJson() => {
        "meta": meta?.toJson(),
        "data": data,
      };
}
