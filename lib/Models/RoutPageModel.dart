
class RoutPageModel {
  String? routePageName;
  String? pageName;
  DateTime? activeTime;

  RoutPageModel();

  RoutPageModel.fromJson(Map<String, dynamic> json) {
    routePageName = json["routePageName"] ?? json['routePageName'];
    pageName = json["pageName"] ?? json['pageName'];
    activeTime = json["activeTime"] ?? DateTime.parse(json["activeTime"]);
  }

  Map<String, dynamic> toJson() => {
        "routePageName": this.routePageName,
        "pageName": this.pageName,
        "activeTime":activeTime!.toIso8601String(),
      };
}