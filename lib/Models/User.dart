import 'package:flutter/material.dart';
import 'package:happychat/res/Fonts.dart';

class User {
  int? id;
  String? username;
  String? token;
  String? name;
  Language language = Language.persian;
  TextDirection textDirection = TextDirection.rtl;

  User();
  User.fromJson(Map<String, dynamic>? json) {
    try {
      id = json?["id"];
      name = json?["name"];
      username = json?["username"];
      token = json?["token"];
    } on Exception catch (e) {
      print(e);
    }
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "username": username,
      "token": token,
      "name": name,
    };
  }
}
