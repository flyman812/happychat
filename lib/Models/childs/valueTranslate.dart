class ValueTranslate {
  final String? name;
  final String? value;

  ValueTranslate({
    this.name,
    this.value,
  });

  @override
  String toString() {
    var values = {"key": name, "value":value};
    return values.toString();
  }
}
