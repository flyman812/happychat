class Verification {
  bool? ok;

  Verification();
  Verification.fromJson(Map<String, dynamic>? json) {
    ok = json?["ok"];
  }

  Map<String, dynamic> toJson() => {
        "ok": ok == null ? null : ok,
      };
}
