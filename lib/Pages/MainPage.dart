import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:happychat/Components/HappyInput.dart';
import 'package:happychat/Controllers/LoginController.dart';
import 'package:happychat/res/Translations.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  LoginController lc = Get.put(LoginController());

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return GetBuilder<LoginController>(
      builder: (_) => SafeArea(
        child: GestureDetector(
          onTap: (){
            FocusManager.instance.primaryFocus!.unfocus();
          },
          child: Scaffold(
            body: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                HappyInput(
                  hintText: HappyChatKeys.translateForLocale(
                      HappyChatKeys.enterYourPhoneNumber),
                  textEditingController: _.phoneNumberController,
                  onSubmitted: (value) {},
                  width: width * 0.85,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
