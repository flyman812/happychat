import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:happychat/Components/HappyButton.dart';
import 'package:happychat/Components/HappyInput.dart';
import 'package:happychat/Controllers/LoginController.dart';
import 'package:happychat/Controllers/serviceController.dart';
import 'package:happychat/Utils/PageRoutHandler.dart';
import 'package:happychat/Utils/pageRoutList.dart';
import 'package:happychat/res/Colors.dart';
import 'package:happychat/res/Fonts.dart';
import 'package:happychat/res/Sizing.dart';
import 'package:happychat/res/Translations.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  LoginController lc = Get.put(LoginController());
  ServiceController sc = Get.put(ServiceController());

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return GetBuilder<LoginController>(
      builder: (_) => SafeArea(
        child: GestureDetector(
          onTap: () {
            FocusManager.instance.primaryFocus!.unfocus();
          },
          child: Scaffold(
            backgroundColor: HappyColor.white,
            body: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                colors: [
                  HappyColor.white,
                  HappyColor.white.withOpacity(0.0),
                  HappyColor.lightPink,
                ],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              )),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    flex: 6,
                    child: Center(
                      child: Text(
                        HappyChatKeys.translateForLocale(
                            HappyChatKeys.happyChat),
                        style: TextStyle(
                          color: HappyColor.black,
                          fontFamily: HappyFont.changeFont(),
                          fontSize: HappySizing.s40,
                          fontWeight: HappySizing.fw7,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 9,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16.0),
                          child: Text(
                            HappyChatKeys.translateForLocale(
                                HappyChatKeys.enterPhoneNumber),
                            style: TextStyle(
                              color: HappyColor.black,
                              fontFamily: HappyFont.changeFont(),
                              fontWeight: HappySizing.fw6,
                              fontSize: HappySizing.s16,
                            ),
                          ),
                        ),
                        HappyInput(
                          hintText: HappyChatKeys.translateForLocale(
                              HappyChatKeys.enterYourPhoneNumber),
                          textEditingController: _.phoneNumberController,
                          onSubmitted: (value) {},
                          onChanged: (value) {
                            setState(() {});
                          },
                          width: width * 0.85,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            floatingActionButton: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12.0),
              child: HappyButton(
                width: width * 0.85,
                onPressed: _.phoneNumberController!.text.length < 9
                    ? null
                    : () {
                      FocusManager.instance.primaryFocus!.unfocus();
                        if (!_.progressEnabled) {
                          _.progressEnabled = true;
                          sc
                              .phoneVerification(
                            phoneNumber: _.phoneNumberController!.text,
                          )
                              .then((value) {
                            value.ok == true
                                ? pageRouteHandler(
                                    namePage: NamePages.otpLogin,
                                    typeRoute: TypeRoute.toNamed,
                                  )
                                : printError();
                          });
                        }
                      },
                progressEnabled: _.progressEnabled,
                text: HappyChatKeys.translateForLocale(HappyChatKeys.signIn),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
