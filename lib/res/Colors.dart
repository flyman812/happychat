
import 'package:flutter/material.dart';

class HappyColor {
  static const Color black = Color(0xff243443);
  static const Color grey = Color(0xff413D4B);
  static const Color grey2 = Color(0xff58616A);
  static const Color grey3 = Color(0xff828282);
  static const Color grey4 = Color(0xffC4C4C4);
  static const Color grey5 = Color(0xffE0E0E0);
  static const Color grey6 = Color(0xffF2F2F2); 
  static const Color white = Color(0xffffffff); 
  static const Color lightPink = Color(0xffFCEDEA);
  static const Color pink = Color(0xffF6BEB1);
  static const Color darkPink = Color(0xffEB5757);
  static const Color yellow = Color(0xffFBDEAC);
}