import 'package:get/get.dart';
import 'package:happychat/Controllers/serviceController.dart';

class HappyFont {
  static const String iransansDn = "iransansDn";
  static const String productSans = "ProductSans";
  static const String yekanBakh = "yekanBakh";

  static String changeFont() {
    ServiceController sc = Get.put(ServiceController());
    if (sc.user.language == Language.persian)
      return iransansDn;
    else if (sc.user.language == Language.english)
      return productSans;
    else
      return yekanBakh;
  }
}

enum Language {
  persian,
  english,
}
