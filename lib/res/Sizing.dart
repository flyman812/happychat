import 'package:flutter/material.dart';

class HappySizing {
  // border radius
  static const BorderRadius br8 = BorderRadius.all(Radius.circular(8));

  // font Size
  static const double s40 = 40.0;
  static const double s18 = 18.0;
  static const double s16 = 16.0;
  static const double s14 = 14.0;
  static const double s13 = 13.0;
  static const double s10 = 10.0;


  // font Weigth
  static const FontWeight fw7 = FontWeight.w700;
  static const FontWeight fw6 = FontWeight.w600;
  static const FontWeight fw4 = FontWeight.w400;
  
}
