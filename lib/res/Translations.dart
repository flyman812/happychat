import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'dart:async' show Future;
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:happychat/Models/childs/valueTranslate.dart';
import 'package:happychat/res/urls.dart';

class HappyTranslations extends Translations {
  final Map<String, String> enUS;
  final Map<String, String> faIR;

  HappyTranslations(this.enUS, this.faIR);

  static HappyTranslations fromJson(dynamic json) {
    return HappyTranslations(
      Map<String, String>.from(json["en_US"]),
      Map<String, String>.from(json["fa_IR"]),
    );
  }

  @override
  // TODO: implement keys
  Map<String, Map<String, String>> get keys => {"en_US": enUS, "fa_IR": faIR};
}

class TranslationsProvider {
  Future<HappyTranslations> getTranslations() async {
    Map<String, dynamic> data;
    var jsonText = await rootBundle.loadString(HappyUrls.translations);
    data = json.decode(jsonText);
    final response = HappyTranslations.fromJson(data);
    return response;
  }
}

class HappyChatKeys {
  static const String happyChat = "happyChat";
  static const String back = "back";
  static const String resendCode = "resend_Code";
  static const String writeMessage = "write_message";
  static const String enterOtpCode = "enter_otp_code";
  static const String enterPhoneNumber = "enter_phone_number";
  static const String enterYourPhoneNumber = "enter_your_phone_number";
  static const String incorrectPhoneNumber = "incorrect_phone_number";
  static const String incorrectOtpCode = "incorrect_otp_code";
  static const String signIn = "sign_in";
  static const String dontLoginToThisWorld = "dont_login_to_this_world";
  static const String resendCodeSoon = "resend_code_soon";
  

  static String translateForLocale(String text) {
    return text.tr;
  }

  setTranslate({text, List<ValueTranslate>? values}) {
    List splitedText = text.split(' ');
    for (String item in splitedText) {
      if (item.contains('___')) {
        String key = item.replaceAll('___', '');
        var reg = new RegExp(item);
        String? value = "";
          for (ValueTranslate objValue in values??[]) {
            if (objValue.name == key) {
              value = objValue.value;
              break;
            }
          }
        text = text.replaceAll(reg, value);
      }
    }
    return text;
  }
}
