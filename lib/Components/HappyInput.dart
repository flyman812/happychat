import 'package:flutter/material.dart';
import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';
import 'package:get/get.dart';
import 'package:happychat/Controllers/serviceController.dart';
import 'package:happychat/res/Assets.dart';
import 'package:happychat/res/Colors.dart';
import 'package:happychat/res/Fonts.dart';
import 'package:happychat/res/Sizing.dart';

class HappyInput extends StatelessWidget {
  final double? width;
  final String? hintText;
  final String? errorText;
  final void Function(String)? onSubmitted;
  final void Function(String)? onChanged;
  final TextEditingController? textEditingController;
  HappyInput({
    Key? key,
    this.width,
    this.hintText,
    this.errorText,
    this.textEditingController,
    this.onSubmitted,
    this.onChanged,
  }) : super(key: key);

  ServiceController sc = Get.put(ServiceController());

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: 54,
      decoration: BoxDecoration(
        borderRadius: HappySizing.br8,
      ),
      child: Directionality(
        textDirection: TextDirection.ltr,
        child: TextField(
          controller: textEditingController,
          decoration: InputDecoration(
            
            // icon:
            prefixIcon: Container(
              width: 30,
              padding: EdgeInsets.all(5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: 35,
                    child: ClipRRect(
                      borderRadius: HappySizing.br8,
                      child: Image(
                        image: AssetImage(HappyAssets.iran),
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                  VerticalDivider(
                    color: HappyColor.grey3,
                    width: 1,
                  ),
                ],
              ),
            ),
            fillColor: HappyColor.white,
            border: OutlineInputBorder(
              borderSide: BorderSide(
                color: HappyColor.grey5,
                width: 1,
              ),
              borderRadius: HappySizing.br8,
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: HappyColor.black,
                width: 1,
              ),
              borderRadius: HappySizing.br8,
            ),
            errorBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: HappyColor.darkPink,
                width: 1,
              ),
              borderRadius: HappySizing.br8,
            ),
            disabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: HappyColor.grey3,
                width: 1,
              ),
              borderRadius: HappySizing.br8,
            ),
            hintText: hintText,
            hintStyle: TextStyle(
              color: HappyColor.grey5,
              fontFamily: HappyFont.changeFont(),
              fontSize: HappySizing.s14,
              fontWeight: HappySizing.fw4,
            ),
            errorText: errorText,
          ),
          inputFormatters: [
            MaskedInputFormatter('### ### ####'),
          ],
          keyboardType: TextInputType.phone,
          onSubmitted: onSubmitted,
          onChanged: onChanged,
          
        ),
      ),
    );
  }
}
