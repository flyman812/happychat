import 'package:get/get.dart';
import 'package:happychat/Pages/ChatPage.dart';
import 'package:happychat/Pages/LoginPage.dart';
import 'package:happychat/Pages/MainPage.dart';
import 'package:happychat/Pages/OtpLogin.dart';

List<GetPage> pageRouteList = [
  GetPage(name: NamePages.chatPage, page: () => ChatPage()),
  GetPage(name: NamePages.loginPage, page: () => LoginPage()),
  GetPage(name: NamePages.mainPage, page: () => MainPage()),
  GetPage(name: NamePages.otpLogin, page: () => OtpLogin()),
  // GetPage(name: NamePages.splashScreen, page: () => CartPage()),
];

class NamePages {
  static const chatPage = "/chatPage";
  static const loginPage = "/loginPage";
  static const mainPage = "/mainPage";
  static const otpLogin = "/otpLogin";
  static const splashScreen = "/splashScreen";

  static String foundNamePage({String? name}) {
    Map<String, String> namePages = {
      NamePages.loginPage: "login Page",
      NamePages.chatPage: "chat Page",
      NamePages.mainPage: "main Page",
      NamePages.otpLogin: "OTP Page",
      NamePages.splashScreen: "splash Screen Page",
    };

    // String itemFound = namePages.keys.firstWhere((element) => item == element);
    String? itemFound = namePages[name];

    return itemFound ?? "";
  }
}
