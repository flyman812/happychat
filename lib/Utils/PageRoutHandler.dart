


import 'package:get/get.dart';
import 'package:happychat/Controllers/serviceController.dart';
import 'package:happychat/Models/RoutPageModel.dart';
import 'package:happychat/Utils/pageRoutList.dart';

void pageRouteHandler({
   String? namePage,
  TypeRoute? typeRoute,
  dynamic arguments,
}) async {
  ServiceController sc = Get.put(ServiceController());

  try {
    DateTime dateTime = DateTime.now();
    RoutPageModel routPageModel = new RoutPageModel();

    routPageModel.activeTime = dateTime;
    routPageModel.routePageName =
        namePage != null && typeRoute != TypeRoute.back ? namePage : null;
    routPageModel.pageName = namePage != null && typeRoute != TypeRoute.back
        ? NamePages.foundNamePage(name: namePage)
        : null;

    switch (typeRoute!) {
      case TypeRoute.toNamed:
        Get.toNamed(
          namePage!,
          arguments: arguments ?? arguments,
        );
        break;
      case TypeRoute.offAndToNamed:
        Get.offAndToNamed(
          namePage!,
          arguments: arguments ?? arguments,
        );
        break;
      case TypeRoute.back:
        Get.back();
        break;
      case TypeRoute.offAllNamed:
        Get.offAllNamed(
          namePage!,
          arguments: arguments ?? arguments,
        );
        break;
      case TypeRoute.offNamed:
        Get.offNamed(
          namePage!,
          arguments: arguments ?? arguments,
        );
        break;
      case TypeRoute.changeMainPage:
        break;
    }
    sc.routPageModels.add(routPageModel);
  } on Exception catch (e, s) {
    print(e);
  }
}

enum TypeRoute {
  toNamed,
  offAndToNamed,
  back,
  offAllNamed,
  offNamed,
  changeMainPage,
}
