import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginController extends GetxController {
  //variables
  bool _validated = false;
  TextEditingController? _phoneNumberController = new TextEditingController();
  bool _progressEnabled = false;

  //getters

  bool get progressEnabled => _progressEnabled;

  TextEditingController? get phoneNumberController => _phoneNumberController;

  bool get validated => _validated;

  //setters

  set progressEnabled(bool progressEnabled) {
    _progressEnabled = progressEnabled;
    update();
  }

  set phoneNumberController(TextEditingController? phoneNumberController) {
    _phoneNumberController = phoneNumberController;
    update();
  }

  set validated(bool validated) {
    _validated = validated;
    update();
  }

  //functions

}
