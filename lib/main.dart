import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:happychat/Pages/LoginPage.dart';
import 'package:happychat/Pages/MainPage.dart';
import 'package:happychat/Utils/pageRoutList.dart';
import 'package:happychat/res/Translations.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final translations = await TranslationsProvider().getTranslations();
  runApp(MyApp(translations: translations));
}

class MyApp extends StatelessWidget {
  final HappyTranslations? translations;

  const MyApp({Key? key, this.translations}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Happy Chat',
      debugShowCheckedModeBanner: false,
      themeMode: ThemeMode.light,
      translations: translations,
      getPages: pageRouteList,
      locale: Locale("fa", "IR"),
      theme: ThemeData(
        primarySwatch: Colors.lime,
      ),
      home: LoginPage(),
    );
  }
}
